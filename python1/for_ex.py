#!/usr/bin/env python3

animals = ["cat", "dog", "mouse", "rabbit"]
for a in animals:
    print(a, "has", len(a), "letters.")

scores = [60, 65, 74, 62, 58, 80]
total = 0
for score in scores:
    total = total + score
print("The average score is", total/len(scores))
