#!/usr/bin/env python3

string1 = "Hello"
string2 = "World!"
# A comma can be used to output several items together.
# By default this inserts a space between them on output.
print(string1, string2)
# We can concatentate strings with `+`. Note we need to include the space.
string3 = string1 + " " + string2
print(string3)
# We can also do this directly in the argument of the print function.
print(string1 + " " + string2)
# Comments start with a # symbol if you haven't noticed yet. Anything after
# a # is ignored when the code is run.
