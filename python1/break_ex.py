#!/usr/bin/env python3

n = int(input("Please enter a positive integer: "))
for ii in range(2, n):
    if n % ii == 0:
        print(n, "is not prime.")
        break
else:
    print(n, "is prime.")
