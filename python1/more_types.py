#!/usr/bin/env python3

# Lists are mutable ordered sequences of variables of any type.
options_list = ["a", "b", "c", "d", 5, -6]
for option in options_list:
    print(option)
print()

# Sets are mutable unordered groups of data with unique immutable elements.
options_set = {"a", "b", "c", "d", 5, -6}
for option in options_set:
    print(option)
print()
# Note there is also the frozenset type if you need to create immutable sets.

# Tuples are immutable ordered groups of values formed by a comma separated
# list of values.
options_tuple = "a", "b", "c", "d", 5, -6
# We could also have written this as
# options_tuple = ("a", "b", "c", "d", 5, -6)
for option in options_tuple:
    print(option)
print()
# Tuple expressions are often used to assign several values at once, or when
# a function returns several values at once. E.g.:
a, b, c = 1, 2, 3
print(a, b, c)
print()

# Dicts are groups of key: value pairs. The keys must be unique and immutable,
# but the dict is mutable. They are unordered.
info = {"Alice": 1, "Bob": 0, "Carol": 0.5}
info["Carol"] = 0.6
for name in info:
    print(name, info[name])
