#!/usr/bin/env python3

s = input("Input some text: ")
print("You input:", s)
print("The first character input is", s[0])
# Note indexing starts at 0.

# We can extract ranges of values as follows:
print("The second to fourth character are", s[1:4])
print("The last character input is", s[-1])
print("The first 3 characters are", s[:3])
print("The characters after the second are", s[2:])
print("The last 2 characters are", s[-2:])

print("Converted to uppercase:", s.upper())
print("Converted to lowercase:", s.lower())

# len(s) returns an int giving the number of characters.
print("The number of characters input is", len(s))

# Strings are immutable. The following would generate an error.
#s[2]  = 'c'
