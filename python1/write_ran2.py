#!/usr/bin/env python3

import random

def write_to_file(filename, data):
    """Output the elements of data to the file filename one per line."""
    f = open(filename, "w")
    for item in data:
        f.write(str(item) + '\n')
    f.close()

def gen_random_data(n, centre, width):
    """Return n random numbers from a gaussian using centre and width."""
    random_data = []
    for ii in range(n):
        random_data.append(random.gauss(centre, width))
    return random_data

def main():
    write_to_file("200randoms.dat", gen_random_data(200, 10, 1))

if __name__ == '__main__':
    main()
