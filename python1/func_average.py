#!/usr/bin/env python3

def average(nums):
    """Return the average of a group of numbers."""
    return sum(nums)/len(nums)

test_nums = 1, 2, 3, 4, 5
print("The average of these numbers is", average(test_nums))
